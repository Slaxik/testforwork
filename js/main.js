(function($){
  'use strict';

  jQuery(document).ready(function($) {
    var app = new App({delay: 1500}),
      // опции jbox для всплывающих нотификаторов
      noticeOptions = {
        attributes: {
          x: 'right',
          y: 'bottom'
        },
        theme: 'NoticeBorder',
        color: 'green',
        animation: {
          open: 'slide:bottom',
          close: 'slide:right'
        }
      },
      // кэшируем табы в переменную
      tabs = $('#wrapper > ul.nav-tabs'),
      activeTab = $('> li.active > a', tabs).attr('href').replace('#', ''),
      // заголовки модальных окон при создании и редактировании
      modalText = {
        persons: {create: 'Create person', edit: 'Edit person'},
        users: {create: 'Create user', edit: 'Edit user'},
        positions: {create: 'Create position', edit: 'Edit position'},
        departments: {create: 'Create department', edit: 'Edit department'},
        companies: {create: 'Create company', edit: 'Edit company'},
        button: {create: 'Create', edit: 'Save'}
      };

    app.read();

    // действие создать или сохранить в модальном окне
    $(document).on('click', '.modal[role=dialog] button[role=button]', function(e) {
      var id = $('#create'+app.capitalize(activeTab)+'Modal').data('edit-id'),
        createPromise = $.type(id) !== 'undefined' ? app.create(e, id): app.create(e);

      $.when(createPromise).done(function(data){
        $.jGrowl('Успешно добавленно!', {header:'Добавление!'});
      });
    });

    // редактирование элемента
    $(document).on('click', 'button[name=edit]', function(e) {
      var modal = $('#create'+app.capitalize(activeTab)+'Modal').data('edit', true),
        tableRow = $(e.target).parents('tr');
      modal.data('edit-id', $('td:first', tableRow).text()).modal('show');
      app.setFormData($('form', modal), tableRow);
    });

    // удаление элемента с нотификацией
    $(document).on('click', 'button[name=remove]', function(e){
      if(confirm('Are you sure?')) {
        $(e.target).prop("disabled", true).prev().prop("disabled", true);
        $.when(app.remove(e)).done(function(data){
         $.jGrowl('Данные удалены!', {header:'Удаление!'});
        });
      }
    });

    // срабатывает при переключении вкладки, устанавливает activeTab, делает запрос чтения
    tabs.on('show.bs.tab', function(e) {
      activeTab = $(e.target).attr('href').replace('#', '');
      app.read(activeTab);
    });

    // модальное окно, создание и редактирование
    $('.modal[role=dialog]').on('show.bs.modal', function (e) {
      // если создаем элемент, то очищаем текстовые поля
      var edit = $.type($(this).data('edit')) === 'boolean' ? $(this).data('edit') : false;
      if (!edit) {
        $('form :input', this).val('');
      }
      // устанавливаем заголовок модального окна и button действия в зависимости от режима (создание/редактирование)
      $('h4.modal-title', this).text(edit ? modalText[activeTab].edit : modalText[activeTab].create);
      $('button[role=button]', this).text(edit ? modalText.button.edit : modalText.button.create);
    }).on('shown.bs.modal', function (e) {
      // устанавливаем фокус на первый
      $(':input:first', this).focus();
    }).on('hidden.bs.modal', function (e) {
      // при закрытии устанавливаем edit = false
      $(this).data('edit', false);
      // и удаляем data-edit-id при закрытии
      $.removeData(this, 'edit-id');
    });

  });
})(jQuery);

function App(options) {
  'use strict';

  var $this = this;
  $this.options = {
    delay: 1500
  };

  $this.tables = {
    persons: ['person_id', 'first_name', 'last_name', 'middle_name', 'email', 'phone_number'],
    users: ['user_id', 'nickname', 'department_id', 'person_id', 'position_id', 'super_user'],
    positions: ['position_id', 'position_name', 'salary'],
    departments: ['department_id', 'department_name', 'company_id'],
    companies: ['company_id', 'company_name', 'description', 'logo']
  };

  // Merge options
  $this.options = jQuery.extend(true, $this.options, options);

  $this.getId = function(storage) {
    storage = storage || $('#wrapper > ul.nav-tabs > li.active > a').attr('href').replace('#', '');
    var ns = $.initNamespaceStorage(storage),
    // получаем все ключи из localStorage, сортируем по возрастанию
    keys = jQuery.map(ns.localStorage.keys(), function(val, ind) {
      return parseInt(val);
    }).sort(function (a,b){ return a-b;});
    // возвращаем значение последнего элемента массива + 1, или 1 если массив пуст
    return $.isArray(keys) && keys.length > 0 ? keys.pop() + 1 : 1;
  };

  // capitalize text
  $this.capitalize = function(str) {
    var first = str.substr(0,1).toUpperCase();
    return first + str.substr(1);
  };

  // удаляет row если данные есть / добавляет row если данных нет  
  $this.tableRender = function(storage) {
    var tbody = $('#'+storage+' > table > tbody'),
      emptyHtml = '<tr><td colspan="'+ (this.tables[storage].length + 1) +'" class="text-center">No data</td></tr>';
    
    if (tbody.is(':empty')) {
      tbody.html(emptyHtml);
    } else {
      var noData = $('tr', tbody).children('td.text-center[colspan='+ (this.tables[storage].length + 1) +']');
      if (noData.length) noData.parent('tr').remove();
    }
  };

  // создаем объект с полями
  $this.getFormData = function(form) {
    var arr = form.serializeArray(),
      inputs = $(':input', form),
      fields = {};

    inputs.each(function(i, input) {
      fields[input.name] = input.type === 'checkbox' || input.type === 'radio' ? $(input).is(':checked') : $(input).val();
    });

    return fields;
  };

  // применяем данные к форме
  this.setFormData = function(form, row) {
    var id = row.data('id'),
      fields = row.data('fields');

    $(':input', form).each(function(i, input) {
      if (input.type === 'checkbox' || input.type === 'radio')
        $(input).prop('checked', $.type(fields[input.name]) === 'boolean' ? fields[input.name] : false);
      else
        $(input).val(fields[input.name]);
    });
  };
}

// чтение данных из localStorage
App.prototype.read = function(storage) {
  storage = storage || $('#wrapper > ul.nav-tabs > li.active > a').attr('href').replace('#', '');
  var d = $.Deferred(),
    $this = this;
  setTimeout(function() {
    var ns = $.initNamespaceStorage(storage),
        storageItems = ns.localStorage.get(),
        fields = $this.tables[storage],
        html = '';
    $.each(storageItems, function(id, val) {
      html += '\n<tr role="row" data-fields=\''+JSON.stringify(val)+'\' data-id="'+id+'">';
      $.each(fields, function(i, field) {
        if (i === 0) val[field] = id;
        html += '\t<td>'+ ($.type(val[field]) !== 'undefined' ? val[field] : '') +'</td>';
      });
      html += ['\t<td class="text-center">',
                  '\t\t<button type="button" name="edit" class="btn btn-primary btn-sm">Edit</button>',
                  '\t\t<button type="button" name="remove" class="btn btn-danger btn-sm">Remove</button>',
                '\t</td>'].join('\n');
      html += '\n</tr>';
    });
    $('#'+storage+' > table > tbody').html(html);
    $this.tableRender(storage);
    d.resolve(); 

  }, this.options.delay);

  return d.promise();
};

// добавление в нового поля
App.prototype.add = function(id, val) {
  var $this = this,
    storage = $('#wrapper > ul.nav-tabs > li.active > a').attr('href').replace('#', ''),
    fields = $this.tables[storage],
    row, 
    tbody = $('#'+storage+' > table > tbody'),
    html = '';
  if($.type(val) === 'object' && !$.isEmptyObject(val)) {
    html = '\n<tr role="row" data-fields=\''+JSON.stringify(val)+'\' data-id="'+id+'">';
    $.each(fields, function(i, field) {
      if (i === 0) val[field] = id;
      html += '\t<td>'+val[field]+'</td>';
    });
    html += ['\t<td class="text-center">',
                '\t\t<button type="button" name="edit" class="btn btn-primary btn-sm">Edit</button>',
                '\t\t<button type="button" name="remove" class="btn btn-danger btn-sm">Remove</button>',
              '\t</td>'].join('\n');
    html += '\n</tr>';
    $.each($('tr', tbody), function(i, tr) {
      if($('td:first:contains('+id+')', tr).length)
        row = $(tr);
    });
    if (row)
      row.replaceWith($(html));
    else
      $(html).appendTo(tbody);
    $this.tableRender(storage);
  }
};

// создание или редактирование записи в localStorage
App.prototype.create = function(e, el) {
  e.preventDefault();
  var d = $.Deferred(),
    $this = this;

  $('.modal[role=dialog]:visible').modal('hide');
  setTimeout(function() {
    var storage = $('#wrapper > ul.nav-tabs > li.active > a').attr('href').replace('#', ''),
      ns = $.initNamespaceStorage(storage),
      form = $(e.target).parents('.modal-content').find('form'),
      fields = $this.getFormData(form),
      id = el || $this.getId().toString();

    ns.localStorage.set(id, fields);
    $this.add(id, fields);
    d.resolve({title: 'Success', content: 'ID '+id+' successfully '+(el ? 'updated':'added')+'!', id: id, fields: fields});
  }, this.options.delay);
  
  return d.promise();
};

// удаление записи из localStorage
App.prototype.remove = function(e) {
  var d = $.Deferred(),
    $this = this;

  setTimeout(function() {
    var storage = $('#wrapper > ul.nav-tabs > li.active > a').attr('href').replace('#', ''),
      ns = $.initNamespaceStorage(storage),
      tr = $(e.target).parents('tr'),
      id = tr.children('td:first').text();
    ns.localStorage.remove(id);
    tr.remove();
    $this.tableRender(storage);
    d.resolve({id: id, title: 'Success', content: 'ID '+id+' successfully removed!'});
  }, this.options.delay);

  return d.promise();
};